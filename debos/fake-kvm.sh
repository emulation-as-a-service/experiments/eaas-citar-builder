#!/bin/sh

if test -e /dev/kvm; then exit; fi

touch /dev/kvm

if ! test -e /usr/bin/real-qemu-system-x86_64; then
  mv /usr/bin/qemu-system-x86_64 /usr/bin/real-qemu-system-x86_64
fi

cat > /usr/bin/qemu-system-x86_64 << "EOF"
#!/bin/sh
i="$#"; while test "$i" -gt 0; do
  case "$1" in
    "-enable-kvm")
      :;;
    "-cpu")
      shift; i="$((i-1))";;
    *)
      set -- "$@" "$1";;
  esac
  shift; i="$((i-1))"
done
exec /usr/bin/real-qemu-system-x86_64 "$@"
EOF

chmod +x /usr/bin/qemu-system-x86_64
